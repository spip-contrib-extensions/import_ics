<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/import_ics.git

return [

	// A
	'archive' => 'archive',
	'archiver' => 'archivé',

	// C
	'cextras_editables' => 'Les champs  ATTENDEE, ORIGIN, NOTE des évènements sont proposés à l’édition',
	'cfg_attention' => 'Attention : les modifications effectuées sur ce formulaire n’affecteront pas les almanachs déjà créés et les évènements associés.',
	'cfg_configurer' => 'Configurer l’import de fichiers ICS',

	// D
	'depublier_anciens_evts' => 'Dépublier les anciens évènements',
	'depublier_anciens_evts_explication' => 'Cocher cette case si vous souhaitez que les évènements qui ne sont plus présents dans un flux distant soient automatiquement basculés à en « archivés »',

	// E
	'email_erreur_corps' => 'Le(s) almanach(s) suivant(s) n’arrive(nt) pas à être synchronisé(s). Vérifier les flux ICAL sources.@liste@',
	'email_erreur_sujet' => 'Erreur dans la synchronisation d’almanach',

	// I
	'import_ics_titre' => 'Import_ics',
];
