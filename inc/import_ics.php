<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_once(_DIR_PLUGIN_ICALENDAR . '/vendor/kigkonsult/icalcreator/autoload.php');
include_spip('inc/distant');
/**
 * Importation ou synchronisation d'un almanach
 **/

include_spip('inc/autoriser');
include_spip('action/editer_objet');
include_spip('action/editer_liens');
include_spip('inc/config');
include_spip('import_ics_fonctions');
include_spip('inc/filtres_ecrire');
if (!defined('_IMPORT_ICS_DEPUBLIER_ANCIENS_EVTS')) {
	define('_IMPORT_ICS_DEPUBLIER_ANCIENS_EVTS', '');
}
/**
 * Fonction qui trouve tous les évènements associés à un almanach, sauf les archivés (sauf si on demande explicitement)
 **/
function trouver_evenements_almanach($id_almanach, $champs = 'uid,id_evenement', $tous = false) {
	if ($tous) {
		$liens = sql_allfetsel(
			$champs,
			'spip_evenements
			INNER JOIN spip_almanachs_liens AS L
			ON id_evenement = L.id_objet AND L.id_almanach=' . intval($id_almanach)
		);
	}
	else {
		$liens = sql_allfetsel(
			$champs,
			'spip_evenements
			INNER JOIN spip_almanachs_liens AS L
			ON id_evenement = L.id_objet AND L.id_almanach=' . intval($id_almanach),
			'statut!=' . sql_quote('archive')
		);
	}
	return $liens;
}

function importer_almanach($id_almanach, $url, $id_article, $decalage, $dtstart_exclu = false, $dtend_inclus = false) {
	// Début de la récupération des évènements

	$config = [\Kigkonsult\Icalcreator\Vcalendar::UNIQUE_ID => ''];

	$cal = new \Kigkonsult\Icalcreator\Vcalendar($config);
	$content = recuperer_url(trim($url));
	$content = import_ics_nettoyer_ical($content['page']);

	try {
		$cal->parse($content);
		$statut = sql_getfetsel('statut', 'spip_almanachs', "`id_almanach`=$id_almanach");
		$liens = trouver_evenements_almanach($id_almanach);

		// rechercher les mots clefs lié à un objet_modifier
		$mots = lister_objets_lies('spip_mots', 'spip_almanachs', $id_almanach, 'spip_mots');

		// on definit un tableau des uid présentes dans la base
		$uid = [];
		foreach ($liens as $u) {
			$uid[$u['id_evenement']] = $u['uid'];
		};
		$les_uid_distant = [];
		while ($comp = $cal->getComponent()) {
			if (get_class($comp) !== 'Kigkonsult\Icalcreator\Vevent') {
				continue;
			}
			// Ne pas importer les événements marqués comme privés
			$classe = $comp->getClass();
			if (defined('_IMPORT_ICS_PUBLIC_SEULEMENT') && ($classe === \Kigkonsult\Icalcreator\Vcalendar::P_IVATE)) {
				continue;
			}

			//vérifier l'existence et l'unicité
			$uid_distant = $comp->getUid();#uid de l'evenement

			// Ajouter à la liste des uid_distants du flux courants
			if (isset($les_uid_distant[$uid_distant])) {
				$les_uid_distant[$uid_distant]++;
			} else {
				$les_uid_distant[$uid_distant] = 0;
			}

			if (in_array($uid_distant, $uid)) {//si l'uid_distant est présente dans la bdd, alors on teste si l'evenement a été modifié à distance
				$id_evenement = import_ics_evenement_distant_modifie($comp, $uid_distant,$les_uid_distant[$uid_distant]);
				if ($id_evenement) {
					$champs_sql = evenement_ical_to_sql($comp, $decalage, $dtstart_exclu, $dtend_inclus);
					autoriser_exception('evenement', 'modifier', $id_evenement);
					objet_modifier('evenement', $id_evenement, $champs_sql);
					autoriser_exception('evenement', 'modifier', $id_evenement, false);
					spip_log("Mise à jour de l'évènement $id_evenement, almanach $id_almanach", 'import_ics' . _LOG_INFO);
				}
			} else {
				//l'evenement n'est pas dans la bdd, on va l'y mettre
				importer_evenement($comp, $id_almanach, $id_article, $decalage, $statut, $mots, $dtstart_exclu, $dtend_inclus);
			};
		}
		if (_IMPORT_ICS_DEPUBLIER_ANCIENS_EVTS === 'on' ||  lire_config('import_ics/depublier_anciens_evts') === 'on') {
			depublier_ancients_evts($uid, array_keys($les_uid_distant), $id_article);
		}

		// mettre à jour les infos de synchronisation. Le champ derniere_synchro n'est pas un champ éditable, donc on passe par sql_update et pas par editer_objet
		sql_update('spip_almanachs', ['derniere_synchro' => 'NOW()'], 'id_almanach=' . intval($id_almanach));
	} catch (\Exception $e) {
		spip_log("Erreur lors de l'analyse de l'url $url (almanach $id_almanach)", 'import_ics' . _LOG_ERREUR);
		sql_update('spip_almanachs', ['derniere_erreur' => 'NOW()', 'contenu_derniere_erreur' => sql_quote(json_encode($e)) ], 'id_almanach=' . intval($id_almanach));
	}
		return;
}

/**
 * Dépublier (archiver) les anciens évènements
 **/
function depublier_ancients_evts($les_uid_local, $les_uid_distant, $id_article) {
	$diff = array_diff($les_uid_local, $les_uid_distant);
	$print_local = print_r($les_uid_local, true);
	$print_distant = print_r($les_uid_distant, true);
	spip_log("UID local:$print_local;UID_distant:$print_distant", 'import_ics' . _LOG_INFO);
	foreach ($diff as $id_evenement => $uid) {
		autoriser_exception('instituer', 'evenement', $id_evenement);
		autoriser_exception('modifier', 'article', $id_article);
		objet_instituer('evenement', $id_evenement, ['statut' => 'archive']);
		spip_log("Archivage de l'évènement $id_evenement (uid:$uid)", 'import_ics' . _LOG_INFO);
		autoriser_exception('instituer', 'evenement', $id_evenement, false);
		autoriser_exception('modifier', 'article', $id_article, false);
	}
}
/**
 * Importation d'un événement dans la base
 **/
function importer_evenement($objet_evenement, $id_almanach, $id_article, $decalage, $statut, $mots, $dtstart_exclu, $dtend_inclus = false) {
	$champs_sql = array_merge(
		evenement_ical_to_sql($objet_evenement, $decalage, $dtstart_exclu, $dtend_inclus),
		[
			'id_article' => $id_article,
			'date_creation' => date('Y-m-d H:i:s')
		]
	);

	# création de l'evt
	autoriser_exception('creer', 'evenement', '');
	autoriser_exception('modifier', 'article', $id_article);

	$id_evenement = objet_inserer('evenements', $id_article, $champs_sql);
	autoriser_exception('instituer', 'evenement', $id_evenement);

	objet_instituer('evenement', $id_evenement, ['statut' => $statut]);
	// lier les mots
	objet_associer(
		['mot' => $mots],
		['evenement' => $id_evenement]
	);
	autoriser_exception('creer', 'evenement', '', false);
	autoriser_exception('modifier', 'article', $id_article, false);
	autoriser_exception('instituer', 'evenement', $id_evenement, false);


	#on associe l'événement à l'almanach
	objet_associer(['almanach' => $id_almanach], ['evenement' => $id_evenement], ['vu' => 'oui']);
	spip_log("Import de l'évènement $id_evenement, almanach $id_almanach", 'import_ics' . _LOG_INFO);
}

/**
 * Récupérer les propriétés d'un evenements de sorte qu'on puisse en faire la requete sql
 * @param \vevent $objet_evenement un objet de classe vevent
 * @param array $decalage un tableau decrivant les éventuels décalage horaire à appliquer
 * @param bool $dtstart_exclu pour signaler si la date de fin est excluse (normalement, si la norme est respectée, non)
 * @param bool $dtend_inclus pour signaler si la date de fin est incluse (normalement, si la norme est respectée, non)
 * @return array un tableau des champs sql à insérer/modifier, après passage dans le pipeline evenement_ical_to_sql
 **/
function evenement_ical_to_sql($objet_evenement, $decalage, $dtstart_exclu = false, $dtend_inclus = false) {
	//on recupere les infos de l'evenement dans des variables

	$uid_distant = $objet_evenement->getUid();#uid de l'evenement
	$attendee = $objet_evenement->getAttendee(); #nom de l'attendee
	$lieu = $objet_evenement->getLocation();#récupération du lieu
	$summary_array = $objet_evenement->getSummary(true); #summary est un array on recupere la valeur dans l'insertion attention, summary c'est pour le titre !
	$titre_evt = str_replace('SUMMARY:', '', $summary_array['value']);
	$url = $objet_evenement->getUrl();#on récupère l'url de l'événement pour la mettre dans les notes histoire de pouvoir relier à l'événement original
	$descriptif_array = $objet_evenement->getDescription(true);
	$organizer = $objet_evenement->getOrganizer();#organisateur de l'evenement
	$last_modified_distant = $objet_evenement->getLastmodified();
	if ($last_modified_distant === false) {
		$last_modified_distant = -1;
	} else {
		$last_modified_distant = $last_modified_distant->getTimestamp();
	}
	$sequence_distant = $objet_evenement->getSequence();
	if (is_null($sequence_distant)) {
		$sequence_distant = 0;
	}


	//données de localisation de l'évenement
	$localisation = $objet_evenement->getGeo();#c'est un array array( "latitude"  => <latitude>, "longitude" => <longitude>))
	$latitude = $localisation['latitude'] ?? '';
	$longitude = $localisation['longitude'] ?? '';


	// Dates de début et de fin
	$end_all_day = false;
	$dtstart_array = $objet_evenement->getDtstart(true);
	list ($date_debut,$start_all_day) = date_ical_to_sql($dtstart_array, $decalage);
	#les 3 lignes suivantes servent à récupérer la date de fin et à la mettre dans le bon format
	$dtend_array = $objet_evenement->getDtend(true);
	if ($dtend_array !== false) {
		list ($date_fin,$end_all_day) = date_ical_to_sql($dtend_array, $decalage);
	} else {
		$duration_array = $objet_evenement->getDuration(true);
		$decalageheure = 0;
		if ($duration_array !== false && $duration_array['value'] <> '') {
			$date_deb = date_ical_to_sql($dtstart_array, '', true);
			$date_ete = intval(affdate($date_deb, 'I'));//Est-on en heure d'été?
			if (!$start_all_day && is_array($decalage) && isset($decalage['ete']) && isset($decalage['hiver'])) {
				if ($date_ete) {
					$decalageheure = $decalage['ete'];
				} else {
					$decalageheure = $decalage['hiver'];
				}
			}
			$duree_seconde = ($duration_array->value->h + $decalageheure) * 60 * 60 + $duration_array->value->i * 60 + $duration_array->value->s;
			$date_fin = import_ics_date_sql_shift($date_deb, "$duree_seconde second");
		} else {
			$date_fin = $date_debut;
			$end_all_day = $start_all_day;
		}
	}

	// Est-ce que l'evt dure toute la journée?
	if ($end_all_day && $start_all_day) {
		$horaire = 'non';
		if (!$dtend_inclus) {
			$date_fin = import_ics_date_sql_shift($date_fin, '-1 DAY');
			;// Si un évènement dure toute la journée du premie août, le flux ICAL indique pour DTEND le 2 août (cf http://www.faqs.org/rfcs/rfc2445.html). Par contre le plugin agenda lui dit simplement "evenement du 1er aout au 1er aout, sans horaire". D'où le fait qu'on décale $date_fin par rapport aux flux originel.
		}
		if ($dtstart_exclu) {
			$date_debut = import_ics_date_sql_shift($date_debut, '+1 DAY');
			;// Si un évènement dure toute la journée du premie août, le flux ICAL indique pour DTSTARRT le 1 août (cf http://www.faqs.org/rfcs/rfc2445.html). Certains flux ICAL (par ex. de google) sont erronnées.
		}
	}
	else {
		$horaire = 'oui';
	}

	$sql = [
		'date_debut' => $date_debut,
		'date_fin' => $date_fin,
		'titre' => $titre_evt,
		'descriptif' => import_ics_correction_retour_ligne(isset($descriptif_array['value']) ? $descriptif_array['value'] : ''),
		'lieu' => import_ics_correction_retour_ligne($lieu),
		'horaire' => $horaire,
		'attendee' => str_replace('MAILTO:', '', $attendee),
		'id_evenement_source' => '0',
		'uid' => $uid_distant,
		'sequence' => $sequence_distant,
		'last_modified_distant' => $last_modified_distant,
		'hash_distant' => import_ics_hash_vevent($objet_evenement),
		'notes' => $url
	];

	// Les propriétés X- (fournies par le plugin agenda > 3.34 si demandée expressement)
	$xprops = [];
	while ($prop = $objet_evenement->getXprop()) {
		$xprops[$prop[0]] = $prop[1];
	}
	$champs_x = ['adresse', 'places', 'inscription'];
	if (test_plugin_actif('cextras')) {
		include_spip('cextras_pipelines');
		$cextras = champs_extras_objet('spip_evenements');
		$cextras = array_keys($cextras);
		$champs_x = array_merge($champs_x, $cextras);
	}


	foreach ($champs_x as $champ) {
		$xchamp = 'X-' . strtoupper($champ);
		if (isset($xprops[$xchamp])) {
			$sql[$champ] = $xprops[$xchamp];
		}
	}
	// Passage au pipeline
	return pipeline(
		'evenement_ical_to_sql',
		[
			'data' => $sql,
			'args' => [
				'objet_evenement' => $objet_evenement,
				'decalage' => $decalage,
				'dtstart_exclu' => $dtstart_exclu,
				'dtend_inclus' => $dtend_inclus,
				'xprops' => $xprops, // On le passe car c'est pénible à parcourir, on qu'on a avancer l'itérateur de toute facon
				'champs_x' => $champs_x // idem
			]
		]
	);
}

/**
 * Remplace les \n littéral des ICS par des retours lignes.
 * @param string $texte
 * @return string
 **/
function import_ics_correction_retour_ligne($texte) {
	return str_replace('\n', "\n", $texte);
}

/**
 * Prend une date au format sql
 * la decale
 * et la retourne au format sql
 * @param string $date au format sql
 * @param string $shift le decalage à appliquer, selon la norme PHP
 * @return $date au format sql
 **/
function import_ics_date_sql_shift($date, $shift) {
	$date = new DateTime($date);
	$date->Modify($shift);
	return $date->format('Y-m-d H:i:s');
}

/**
 * Nettoyer le fichier ical avant de parser.
 * Pour éviter différentes scories possibles non gérés par la librairie.
 * Cette fonction pour l'instant ne fait rien car les deux soucis signalés ont été corrigé.
 * A l'avenir elle pourrait être complété en attendant la maj de la librairie.
 * Ne pas oublier dans ce cas de créer des jeux de tests.
 * Nettoyer le fichier ical avant de parser, pour éviter différentes scories possible
 * @param string $ical
 * @return string $ical
**/
function import_ics_nettoyer_ical(string $ical): string {
	$ical = preg_replace('/((\n|\r|\r\n){2,})/', "\n", $ical);
	return $ical;
}

/**
 * Vérifier si un évènement a été modifé
 * @param \Kigkonsult\Icalcreator\Vevent $comp
 * @param str $uid_distant
 * @param int $index_distant -> position de l'evenement dans l'ensemble des evenements avec le même uid sur le flux distant
 * @return int $id_evenement si jamais a changé, sinon 0 (à changer pour PHP 8.2 en int|false)
**/
function import_ics_evenement_distant_modifie(\Kigkonsult\Icalcreator\Vevent $comp, string $uid_distant, int $index_distant): int {
	$test_variation = sql_fetsel(
		'last_modified_distant,sequence,id_evenement,hash_distant',
		'spip_evenements',
		'`uid`=' . sql_quote($uid_distant),
		'',
		'',
		"$index_distant,1"
	);
	$last_modified_local = $test_variation['last_modified_distant'];
	$sequence_local = intval($test_variation['sequence']);
	$id_evenement = $test_variation['id_evenement'];

	$last_modified_distant = $comp->getLastmodified();
	if ($last_modified_distant === false) {
		$last_modified_distant = -1;
	} else {
		$last_modified_distant = $last_modified_distant->getTimestamp();
	}
	$sequence_distant = intval($comp->getSequence());

	$return = 0;
	if ($last_modified_local != $last_modified_distant || $sequence_local != $sequence_distant) {
		$return = $test_variation['id_evenement'];
	} else {
		if ($last_modified_distant == -1) {
			if (import_ics_hash_vevent($comp) != $test_variation['hash_distant']) {
				$return = $test_variation['id_evenement'];
			}
		}
	}
	return $return;
}
/**
 * Retourne un hash md5 d'un evenement vevent, encapsulé dans un pseudo calendrier
 * Sert pour tester si jamais un evenement a été modifié ou pas lorsque l'on n'a pas de
 * SEQUENCE ou de last_modified
 * @param \Kigkonsult\Icalcreator\Vevent $comp
 * @return string hash ou ''
**/
function import_ics_hash_vevent(\Kigkonsult\Icalcreator\Vevent $comp): string {
	if (!$comp->getLastmodified() && !$comp->getSequence()) {
		$cal = new \Kigkonsult\Icalcreator\Vcalendar();
		$cal->setUid('hash');// Pas de variabilité de l'UID
		$comp->setDtstamp(new DateTime('1970-01-01  20:00:00'));// Un dtstamp invariable
		$cal->setComponent($comp);
		return md5($cal->createCalendar());
	} else {
		return '';
	}
}
