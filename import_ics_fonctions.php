<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/filtres');
/*
 ** À partir d'un tableau de propriété de date ical, retourne deux infos:
 ** 1. Date formatée en sql
 ** 2. booleen pour savoir si toute la journée
 ** Possibilité de ne retourner qu'une info si on seulement_date à True
 */
function date_ical_to_sql($date, $decalage = [], $seulement_date = false) {
	$params = [];
	$all_day = false;
	if (gettype($date) === 'object' && get_class($date) == 'DateTime') {
		$value = $date;
	} elseif (isset($date['value'])) {
		$value = $date['value'];
		$params = $date['params'] ?? '';
	} else {
		$value = $date;
	}
	if (is_array($value)) {
		$value = new DateTime($value['date'], new DateTimeZone($value['timezone'] ?? $value['tz']));
	}
	if (!$date) {
		return '';
	}
	if (is_array($params) && in_array('DATE', $params)) {
		$all_day = true;
	} 	
	$date_sql = $value->format('Y-m-d H:i:s');
	$date_ete = intval(affdate($date_sql, 'I'));//Est-on en heure d'été?, on remarque le passage par affdate plutot que par une modif du timezone de value pour eviter des effets de double décalage. Note que sans doute on aurait pu se passer de tout ce schmilblick si on utilisait les proprités de Timezone du fichier ical... mais ce qui est fait est fait, et si on change maintenant cela va foutre le boxon partout...
	if (
		!$all_day && is_array($decalage)
		&& isset($decalage['ete'])
		&& isset($decalage['hiver'])
	) {
		if ($date_ete) {
			$decalage = $decalage['ete'];
		} else {
			$decalage = $decalage['hiver'];
		}
		$date_sql = import_ics_date_sql_shift($date_sql, "$decalage HOUR");
	}
	if ($seulement_date) {
		return $date_sql;
	} else {
		return [$date_sql,$all_day];
	}
}
