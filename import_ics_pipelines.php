<?php

/**
 * Utilisations de pipelines par Import_ics
 *
 * @plugin     Import_ics
 * @copyright  2013
 * @author     Amaury
 * @licence    GNU/GPL
 * @package    SPIP\Import_ics\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Optimiser la base de données en supprimant
 * les almanachs à la poubelle
 * et les liens orphelins depuis et vers les almanachs
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function import_ics_optimiser_base_disparus($flux) {

	include_spip('action/editer_liens');

	$res = sql_select(
		'id_almanach AS id',
		'spip_almanachs',
		'statut=' . sql_quote('poubelle')
	);

	$flux['data'] += optimiser_sansref('spip_almanachs', 'id_almanach', $res);
	$flux['data'] += objet_optimiser_liens(['almanach' => '*'], '*');
	$flux['data'] += objet_optimiser_liens(['mot' => '*'], ['almanach' => '*']);

	return $flux;
}



function import_ics_taches_generales_cron($taches_generales) {
	$taches_generales['import_ics_synchro'] = 60 * 5;/*Regarder toutes les 5 minutes s'il y a un flux mise à jour il y a plus de une 1h30*/
$taches_generales['import_ics_alerte_erreur'] = 60 * 60 * 24 * 7;/*Regarder tous les 7 jours s'il y a des flux en erreurs*/
return $taches_generales;
}

function import_ics_evenement_liaisons_colonne_gauche($flux) {
	$flux['data'] = $flux['data'] . recuperer_fond('prive/objets/infos/evenement_liaisons_almanach', $flux['args']);
	return $flux;
}


/**
 * Synchroniser le statut des evenements lorsqu'on publie/depublie un almanach
 * Synchroniser le statut des almanaches lorsqu'on publie/depublie un evt
 * @param array $flux
 * @return array
 */
function import_ics_post_edition($flux) {

	$table = $flux['args']['table'] ?? '';
	$statut = $flux['data']['statut'] ?? '';
	$statut_ancien = $flux['args']['statut_ancien'] ?? '';
	$id_objet = $flux['args']['id_objet'] ?? '';
	$action = $flux['args']['action'] ?? '';

	/* Première cas: le statut de l'almanach est modifié*/
	if (
		$table === 'spip_almanachs'
		&& $id_objet
		&& $action  === 'instituer'
		&& $statut != $statut_ancien
	) {
		$id_almanach = $id_objet;
		$set = [];
		$where = [];
		switch ($statut) {
		case 'poubelle':
			// on passe aussi tous les evenements associes a la poubelle, sans distinction
			$set['statut'] = 'poubelle';
			break;
		case 'publie':
			// on passe aussi tous les evenements prop en publie
			$set['statut'] = 'publie';
			$where[] = "statut='prop'";
			break;
		case 'prop':
			$set['statut'] = 'prop';
			$where[] = "statut='publie'";
			break;
		}
		if (count($set)) {
			include_spip('action/editer_evenement');
			$res = sql_select('E.id_evenement', 'spip_evenements AS E
				INNER JOIN spip_almanachs_liens AS L
				ON E.id_evenement = L.id_objet AND L.id_almanach=' . intval($id_almanach), $where);
			// et on applique a tous les evenements lies a l'almanach
			while ($row = sql_fetch($res)) {
				evenement_modifier($row['id_evenement'], $set);
				spip_log("Modif de l'almanach $id_almanach => passage auto en statut " . $set['statut'] . ' de l\'évènement ' . $row['id_evenement'], 'import_ics' . _LOG_INFO_IMPORTANTE);
			}
			sql_free($res);
		}
	}
	// Second cas: modification du statut d'un article

	if (
		$table === 'spip_articles'
		&& $id_objet
		&& $action === 'instituer'
		&& $statut != $statut_ancien
	) {
		$id_article = $id_objet;
		$set = [];
		// les almanachs associes a cet article
		$where = ['id_article=' . intval($id_article)];
		switch ($statut) {
		case 'poubelle':
			// on passe aussi tous les almanachs associes a la poubelle, sans distinction
			$set['statut'] = 'poubelle';
			break;
		case 'publie':
			// on passe aussi tous les almanachs prop en publie
			$set['statut'] = 'publie';
			$where[] = "statut='prop'";
			break;
		default:// c'est à dire lors de la bascule de l'article vers refuse / en cours de modification / proposé
			if ($statut_ancien == 'publie') {
				// on depublie aussi tous les almanachs publie
				$set['statut'] = 'prop';
				$where[] = "statut='publie'";
			}
			break;
		}
		if (count($set)) {
			include_spip('inc/autoriser');
			include_spip('action/editer_objet');
			$res = sql_select('id_almanach', 'spip_almanachs', $where);
			// et on applique a tous les almanach lies a l'article
			while ($row = sql_fetch($res)) {
				$id_almanach = $row['id_almanach'];
				autoriser_exception('instituer', 'almanach', $id_almanach);
				objet_instituer('almanach', $id_almanach, $set);
				spip_log("Modif de l'article $id_article => passage auto en statut " . $set['statut'] . ' de l\'almanach ' . $row['id_almanach'], 'import_ics' . _LOG_INFO_IMPORTANTE);
				autoriser_exception('instituer', 'almanach', $id_almanach, false);
			}
		}
	}

	return $flux;
}


/**
 * Inserer les infos d'almanach sur les articles
 *
 * @param array $flux
 * @return array
 */
function import_ics_affiche_milieu($flux) {
	$e = trouver_objet_exec($flux['args']['exec']);
	$out = false;
	if (is_array($e) && $e['type'] === 'article' && $e['edition'] === false) {
		$out = recuperer_fond('prive/objets/contenu/article-almanachs', $flux['args']);
	}

	if ($out) {
		if ($p = strpos($flux['data'], '<div id="agenda">')) {
			$flux['data'] = substr_replace($flux['data'], $out, $p, 0);
		}
		elseif ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $out, $p, 0);
		} else {
			$flux['data'] .= $out;
		}
	}
	return $flux;
}

/**
 * Modifier les mots-clé liés à un évènement
 * lorsque l'almanach de cet évènement voit ses mot-clé modifiés
 * @param array $flux
 * @return array
 */
function import_ics_post_edition_lien($flux) {
	if ($flux['args']['objet_source'] === 'mot' && $flux['args']['objet'] === 'almanach') {//si on modifie la liaison mot/objet
		// ce qui a été modifié
		$id_mot = $flux['args']['id_objet_source'];
		$id_almanach = $flux['args']['id_objet'];
		$action = $flux['args']['action'];

		// chercher les evenement liés à l'almanach
		include_spip('inc/import_ics');
		$evenements = trouver_evenements_almanach($id_almanach, 'id_evenement', true);
		foreach ($evenements as $evt) {
			$id_evenement = $evt['id_evenement'];
			if ($action == 'delete') {
				objet_dissocier(
					['mot' => $id_mot],
					['evenement' => $id_evenement]
				);
				spip_log("Dissociation du mot-clef $id_mot de l'évènement $id_evenement suite à modif de l'almanach $id_almanach", 'import_ics' . _LOG_INFO_IMPORTANTE);
			}
			elseif ($action == 'insert') {
				objet_associer(
					['mot' => $id_mot],
					['evenement' => $id_evenement]
				);
				spip_log("Association du mot-clef $id_mot de l'évènement $id_evenement suite à modif de l'almanach $id_almanach", 'import_ics' . _LOG_INFO_IMPORTANTE);
			}
		}
	}
	return $flux;
}


/** Déclarer les almanachs
 * au plugin corbeille
 * @param array $flux;
 * @return array $flux;
 **/
function import_ics_corbeille_table_infos($flux) {
	$flux['almanachs'] = [
		'statut' => 'poubelle',
		'table' => 'almanachs',
		'tableliee' => ['spip_almanachs_liens']
	];
	return $flux;
}
