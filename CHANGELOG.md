# Changelog
## 6.2.1 - 2024-02-10

### Fixed

- Déclarer les chaînes de langue sur forme de fichier qui retourne les choses
- #12 Ne pas afficher la date distante si ce n'est pas un évènement distant

## 6.1.1 - 2024-04-22

### Fixed

- #10 Pas d'erreur fatal lorsque la constante `_IMPORT_ICS_PUBLIC_SEULEMENT` est activée
## 6.1.0 - 2023-07-23

### Added

- Gestion des flux ICAL qui ne proposent ni `SEQUENCE` ni `LAST-MODIFIED` (type  all-in-one-events [WORDPRESS])
### Fixed

- Importer les fichiers qui ont des lignes vides

## 6.0.1 - 2023-07-19

### Fixed

- Bug SQL à l'install

## 6.0.0 - 2023-07-02

### Changed

- Suppression compatiblité SPIP < 4.1
- Compatible 4.2
- Utilise le plugin icalendar v1

## 5.2.0 - 2022-12-01

### Added

- Ajout de la constante `_IMPORT_ICS_PUBLIC_SEULEMENT` pour ne pas importer les évènements avec la classe `PRIVATE`

### Fixed

- #4 Pouvoir insérer les évènements lorsque le hit est provoqué par une personne non connectée
- Lier les évènements importés aux bons mots

## 5.1.0 - 2022-05-26

### Added

- Option pour dire que le DTSTART n'est pas dans l'évènement
- Option pour rendre éditable les champs extras historiquement ajoutés aux événements : ORIGIN, ATTENDEE, NOTES:
	- Par défaut désactivée
	- Sauf si on n'avait déjà le plugin installé

### Fixed

- Ne pas tenter d'importer autre chose qu'un évènement
- Au premier import d'un almanach, après validation du formulaire, utiliser aussi les options sur DTSTART/DTEND
