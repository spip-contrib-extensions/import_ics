<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_IMPORT_ICS_DEPUBLIER_ANCIENS_EVTS')) {
	define('_IMPORT_ICS_DEPUBLIER_ANCIENS_EVTS', '');
}

/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies
 **/
function formulaires_configurer_import_ics_saisies_dist() {
	$saisies = [
		[
			'saisie' => 'explication',
			'options' => [
				'texte' => '<:import_ics:cfg_attention:>',
				'class' => 'attention',
				'nom' => 'attention'
			]
		],
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'articlepardefaut',
				'label' => '<:configureragenda:label_articlepardefaut:>',
			],
			'verifier' => [
				[
					'type' => 'entier',
					'options' => [
						'min' => 1
					]
				]
			]
		],
		[
			'saisie' => 'case',
			'options' => [
				'nom' => 'depublier_anciens_evts',
				'label_case' => '<:import_ics:depublier_anciens_evts:>',
				'conteneur_class' => 'pleine_largeur',
				'valeur_forcee' => _IMPORT_ICS_DEPUBLIER_ANCIENS_EVTS,
				'explication' => '<:import_ics:depublier_anciens_evts_explication:>'
				. (in_array(_IMPORT_ICS_DEPUBLIER_ANCIENS_EVTS, ['on', 'off']) ? ' <:import_ics:v_php:>' : ''),
				'disable' => (in_array(_IMPORT_ICS_DEPUBLIER_ANCIENS_EVTS, ['on', 'off']) ? 'on' : '')
			]
		],
		[
			'saisie' => 'case',
			'options' => [
				'nom' => 'cextras_editables',
				'label_case' => '<:import_ics:cextras_editables:>',
				'conteneur_class' => 'pleine_largeur',
			]
		]
	];
	return $saisies;
}
