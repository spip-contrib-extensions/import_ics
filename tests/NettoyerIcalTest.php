<?php

namespace Spip\ImportIcs\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers import_ics_nettoyer_ical
 * @covers saisies_tester_condition_afficher_si()
 * @covers saisies_evaluer_afficher_si()
 * @internal
 */

class NettoyerIcalTest extends TestCase {
	public static function setUpBeforeClass(): void {
		require_once dirname(__DIR__) . '/inc/import_ics.php';
	}

	public static function dataNettoyerIcal() {
		return [
			'lignevide' =>
			[
				"SEQUENCE:1\nSTATUS:CONFIRMED\n",
				"SEQUENCE:1\n\nSTATUS:CONFIRMED\n"
			]
		];
	}

	/**
	 * @dataProvider dataNettoyerIcal
	 */
	public function testNettoyerIcal($expected, $provided) {
		$actual = import_ics_nettoyer_ical($provided);
		$this->assertEquals($expected, $actual);
	}
}
