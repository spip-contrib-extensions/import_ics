<?php

require_once __DIR__ . '/../vendor/autoload.php';

// decorate SPIP…
if (!function_exists('include_spip')) {
	function include_spip() {
	}
	define('_ECRIRE_INC_VERSION', true);
	define('_DIR_PLUGIN_ICALENDAR', '../icalendar');
}

if (!function_exists('spip_log')) {
	define('_LOG_CRITIQUE', false);
	function spip_log() {
	}
}

