<?php

/**
 * Gestion du génie import_ics_alerte_erreur
 *
 * @plugin import_ics pour SPIP
 * @license GPL
 *
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_PERIODE_IMPORT_ICS')) {
	define('_PERIODE_IMPORT_ICS', 1.5 * 3600);
}
include_spip('inc/import_ics');
/**
 * Envoie un email au / à la webmestre pour indiquer les erreurs de synchronisation du genie
 *
 * @genie import_ics_synchro
 *
 * @param int $last
 *     Timestamp de la dernière exécution de cette tâche
 * @return int
 *     Positif : la tâche a été effectuée
 */
function genie_import_ics_alerte_erreur_dist($t) {
	$avec_erreurs = sql_select('id_almanach, titre', 'spip_almanachs', '`derniere_synchro` < `derniere_erreur`');
	$message = '';
	include_spip('inc/utils');
	while ($almanach = sql_fetch($avec_erreurs)) {
		$url = generer_url_ecrire('almanach', 'id_almanach=' . $almanach['id_almanach']);
		$message .= "\r\n- [" . $almanach['titre'] . "->$url] (" . $almanach['id_almanach'] . ')';
	}
	if ($message) {
		$envoyer_mail = charger_fonction('envoyer_mail', 'inc');
		$email = lire_config('email_webmaster');
		$sujet = _T('import_ics:email_erreur_sujet');
		$corps = _T('import_ics:email_erreur_corps', ['liste' => $message]);
		if ($envoyer_mail($email, $sujet, $corps)) {
			return 1;
		} else {
			return 0;
		}
	} else {
		return 1;
	}
}
